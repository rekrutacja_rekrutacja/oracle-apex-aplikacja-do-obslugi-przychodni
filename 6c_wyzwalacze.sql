--sekwencja i wyzwalacz generuj�cy kolejne numery skierowa� (0 na pocz�tku) [wymagany do zachowania logiki BD]
CREATE SEQUENCE skierowanie_numer_SEQ START WITH 10345067080437 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER skierowanie_numer_TRG BEFORE
  INSERT ON skierowanie FOR EACH ROW 
  WHEN (NEW.numer IS NULL) 
  BEGIN :NEW.numer := TO_CHAR(skierowanie_numer_SEQ.NEXTVAL, 'FM000000000000000000000000');
END;
/

--sprawdzenie poprawno�ci dzia�ania wyzwalacza skierowanie_numer_TRG i sekwencji skierowanie_numer_SEQ
INSERT INTO skierowanie (opis, specjalizacja_id, wizyta_id) VALUES ('testowy insert 1', 1, 1);
INSERT INTO skierowanie (opis, specjalizacja_id, wizyta_id) VALUES ('testowy insert 2', 1, 1);
INSERT INTO skierowanie (opis, specjalizacja_id, wizyta_id) VALUES ('testowy insert 3', 1, 1);
SELECT * FROM skierowanie ORDER BY id;
DELETE FROM skierowanie WHERE opis LIKE 'testowy%';



--sekwencja i wyzwalacz generuj�cy kolejne numery recept (1 na pocz�tku) [wymagany do zachowania logiki BD]
CREATE SEQUENCE recepta_numer_SEQ START WITH 100000000003450934853479 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER recepta_numer_TRG BEFORE
  INSERT ON recepta FOR EACH ROW 
  WHEN (NEW.numer IS NULL) 
  BEGIN :NEW.numer := TO_CHAR(recepta_numer_SEQ.NEXTVAL);
END;
/

--sprawdzenie poprawno�ci dzia�ania wyzwalacza recepta_numer_TRG i sekwencji recepta_numer_SEQ
INSERT INTO recepta (wizyta_id) VALUES (1);
INSERT INTO recepta (wizyta_id) VALUES (1);
INSERT INTO recepta (wizyta_id) VALUES (1);
SELECT * FROM recepta ORDER BY id;
DELETE FROM recepta WHERE wizyta_id = 1;



--sekwencja i wyzwalacz generuj�cy kolejne numery skierowa� na zabieg (2 na pocz�tku) [wymagany do zachowania logiki BD]
CREATE SEQUENCE skierzabieg_numer_SEQ START WITH 200000000000234098234061 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER skierzabieg_numer_TRG BEFORE
  INSERT ON skierzabieg FOR EACH ROW 
  WHEN (NEW.numer IS NULL) 
  BEGIN :NEW.numer := TO_CHAR(skierzabieg_numer_SEQ.NEXTVAL);
END;
/

--sprawdzenie poprawno�ci dzia�ania wyzwalacza skierzabieg_numer_TRG i sekwencji skierzabieg_numer_SEQ
INSERT INTO skierzabieg (uwagi, wizyta_id, typzabiegu_id) VALUES ('testowy insert 1', 2, 2);
INSERT INTO skierzabieg (uwagi, wizyta_id, typzabiegu_id) VALUES ('testowy insert 2', 2, 2);
INSERT INTO skierzabieg (uwagi, wizyta_id, typzabiegu_id) VALUES ('testowy insert 3', 2, 2);
SELECT * FROM skierzabieg ORDER BY id;
DELETE FROM skierzabieg WHERE uwagi LIKE 'testowy%';



--edycja widoku zarobki_lekarz (wy��cznie kolumny dodatek)
CREATE OR REPLACE TRIGGER edycja_zarobki_lekarz_TRG
INSTEAD OF UPDATE
ON zarobki_lekarz 
FOR EACH ROW
BEGIN
    IF UPDATING ('dodatek') THEN
      UPDATE lekarz SET dodatek = :new.dodatek WHERE pwz=:new.pwz;
    ELSE
      RAISE_APPLICATION_ERROR(-20000, 'Mo�na edytowa� wy��cznie kolumn� "dodatek"');
    END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania edycji widoku zarobki_lekarz
SELECT * FROM zarobki_lekarz;
UPDATE zarobki_lekarz SET dodatek=dodatek+100 WHERE pwz='8549241';
SELECT * FROM zarobki_lekarz;
UPDATE zarobki_lekarz SET nazwisko='aaaa' WHERE pwz=8549241; --wygeneruje b��d
SELECT * FROM zarobki_lekarz;



--sprawdzenie numeru PESEL podczas dodawania nowego pacjenta (i edycji ju� istniej�cego)
CREATE OR REPLACE TRIGGER sprawdz_pesel_TRG
BEFORE INSERT OR UPDATE ON pacjent FOR EACH ROW
BEGIN
  IF INSERTING OR UPDATING THEN  --tak, niepotrzebne, ale robione dla wprawy
    DBMS_OUTPUT.PUT_LINE(:NEW.pesel);
    IF NOT spr_pesel(:NEW.pesel) THEN
      RAISE_APPLICATION_ERROR(-20000, 'Nieprawid�owy numer PESEL');
    END IF;
  END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania wyzwalacza sprawdz_pesel_trg
SELECT * FROM pacjent;
INSERT INTO pacjent(imie, nazwisko, pesel) VALUES ('Adam', 'Testowy', '27071910320');
INSERT INTO pacjent(imie, nazwisko, pesel) VALUES ('Adam', 'Testowy', '12345678901'); --b��d - rekord nie zostanie dodany
INSERT INTO pacjent(imie, nazwisko, pesel) VALUES ('Adam', 'Testowy', 'abc'); --b��d - rekord nie zostanie dodany
INSERT INTO pacjent(imie, nazwisko, pesel) VALUES ('Adam', 'Testowy', '123'); --b��d - rekord nie zostanie dodany
DELETE FROM pacjent WHERE nazwisko='Testowy';



--archiwizuje przy edycji pacjenta i generuje b��d przy pr�bie usuni�cia (wykorzystuje wcze�niejsz� procedur�)
CREATE OR REPLACE TRIGGER zarchiwizuj_TRG
BEFORE UPDATE OR DELETE ON pacjent FOR EACH ROW
DECLARE
  v_pacjent pacjent%ROWTYPE;
BEGIN
  IF UPDATING THEN
    v_pacjent.id := :OLD.id;
    v_pacjent.imie := :OLD.imie;
    v_pacjent.nazwisko := :OLD.nazwisko;
    v_pacjent.pesel := :OLD.pesel;
    v_pacjent.ubezpieczenie := :OLD.ubezpieczenie;
    v_pacjent.adres_id := :OLD.adres_id;
    
    zarchiwizuj_dane_pacjenta(v_pacjent); --niestety nie mo�na wys�a� samego :OLD
  ELSE
    RAISE_APPLICATION_ERROR(-20000, 'Nie mo�na usuwa� pacjent�w z bazy');
  END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania wyzwalacza zarchiwizuj_TRG
SELECT * FROM pacjent_arch;
SELECT * FROM pacjent;
UPDATE pacjent SET tel='123456789' WHERE id=1;
SELECT * FROM pacjent;
SELECT * FROM pacjent_arch;
DELETE FROM pacjent WHERE id=1; --wygeneruje b��d



--zapobiega dodaniu pacjenta z numerem telefonu, kt�ry nie jest liczb�
--lub zedytowaniu na taki
CREATE OR REPLACE TRIGGER spr_tel_TRG
BEFORE INSERT OR UPDATE ON pacjent FOR EACH ROW
DECLARE
  e_nieprawidlowy_tel_insert EXCEPTION; --niepotrzebne
  e_nieprawidlowy_tel_update EXCEPTION; --robione dla wprawy
  v_tel pacjent.tel%TYPE;
BEGIN
  IF INSERTING THEN
    IF NOT isnumeric(:NEW.tel) AND :NEW.tel IS NOT NULL THEN
      RAISE e_nieprawidlowy_tel_insert;
    END IF;
  ELSIF UPDATING THEN
    IF NOT isnumeric(:NEW.tel) AND :NEW.tel IS NOT NULL THEN --NULL nie jest poprawny dla isnumeric, ale to pole mo�e by� puste
      RAISE e_nieprawidlowy_tel_update;
    END IF;
  END IF;
  EXCEPTION
    WHEN e_nieprawidlowy_tel_insert THEN
      RAISE_APPLICATION_ERROR(-20000, 'Nie mo�na doda� pacjenta z nieprawid�owym numerem telefonu');
    WHEN e_nieprawidlowy_tel_update THEN
      RAISE_APPLICATION_ERROR(-20000, 'Nie mo�na uaktualni� profilu pacjenta - nieprawid�owy numer telefonu');
END;
/

--sprawdzenie poprawno�ci dzia�ania wyzwalacza spr_tel_TRG
INSERT INTO pacjent (imie, nazwisko, tel) VALUES ('Adam', 'Testowy', '123456789');
SELECT * FROM pacjent;
INSERT INTO pacjent (imie, nazwisko, tel) VALUES ('Adam', 'Testowy', '123'); --b��d
SELECT * FROM pacjent;
INSERT INTO pacjent (imie, nazwisko) VALUES ('Adam', 'Testowy'); --bez telefonu
SELECT * FROM pacjent;
UPDATE pacjent SET tel = '987654321' WHERE imie='Adam' AND nazwisko='Testowy';
SELECT * FROM pacjent;
UPDATE pacjent SET tel = '123' WHERE imie='Adam' AND nazwisko='Testowy';
SELECT * FROM pacjent;
UPDATE pacjent SET tel = NULL WHERE imie='Adam' AND nazwisko='Testowy';
SELECT * FROM pacjent;



--po dodaniu nowego pacjenta wypisz procentowy udzia� p�ci (wreszcie jaki� AFTER)
CREATE OR REPLACE TRIGGER udzial_procentowy_TRG
AFTER INSERT ON pacjent --i nie FOR EACH ROW
BEGIN
  udzial_plci;
END;
/

--sprawdzenie poprawno�ci dzia�ania wyzwalacza udzial_procentowy_TRG
EXECUTE udzial_plci;
INSERT INTO pacjent(imie, nazwisko, pesel) VALUES ('Adam', 'Testowy', '71062605933');



--je�eli ilo�� jakiejs szczepionki spadnie poni�ej 10 sztuk, jej nazwa jest wstawiana do tabeli zamowi�
--(kt�ra jest w za�o�eniu monitorowana przez zewn�trzny program)
CREATE OR REPLACE TRIGGER zamownienie_TRG
AFTER UPDATE ON szczepionka FOR EACH ROW
DECLARE
  v_n INT;
BEGIN
  IF :NEW.ilosc < 10 THEN
    SELECT COUNT(nazwa) INTO v_n FROM zamowic WHERE nazwa = :NEW.nazwa;
    IF v_n = 0 THEN --tak unikamy duplikat�w bez rzucania wyj�tkiem
      INSERT INTO zamowic(nazwa) VALUES (:NEW.nazwa);
    END IF;
  END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania wyzwalacza zamownienie_TRG
SELECT * FROM zamowic;
UPDATE szczepionka SET ilosc = 5 WHERE id IN (1, 4, 6);
SELECT * FROM zamowic;
UPDATE szczepionka SET ilosc = 3 WHERE id IN (1, 4, 6);
SELECT * FROM zamowic; --nadal te same 3 szczepionki