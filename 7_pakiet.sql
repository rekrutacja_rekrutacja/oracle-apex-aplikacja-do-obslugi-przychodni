CREATE OR REPLACE PACKAGE przychodnia AS
  --publiczny typ pomocniczy do przeci��onej funkcji isnumeric
   TYPE t_lista IS TABLE OF VARCHAR2(50);

  FUNCTION isnumeric (p_napis CHAR) RETURN BOOLEAN;
  FUNCTION isnumeric (p_napisy T_LISTA) RETURN BOOLEAN; --funkcja przeci��ona - sprawdza ca�� list� napis�w
  FUNCTION spr_pesel(p_pesel CHAR) RETURN BOOLEAN;
  FUNCTION czy_istnieje(p_nazwa_tabeli VARCHAR2) RETURN BOOLEAN;
  FUNCTION jaka_plec(p_pesel pacjent.pesel%TYPE) RETURN CHAR;
  FUNCTION data_urodzenia(p_pesel pacjent.pesel%TYPE) RETURN DATE;
  
  PROCEDURE zarchiwizuj_dane_pacjenta(p_pacjent pacjent%ROWTYPE);
  PROCEDURE zarchiwizuj_dane_pacjenta(p_pacjent_id pacjent.id%TYPE); --przeci��ona procedura (ta nie zadzia�a w wyzwalaczu (zarchiwizuj_TRG) - ORA-04091)
  PROCEDURE udzial_plci;
  PROCEDURE podwyzka_piel (p_podwyzka IN OUT NUMERIC);
  PROCEDURE odejmij_szczepionke (p_nazwa IN szczepionka.nazwa%TYPE, p_komunikat OUT VARCHAR2);
  PROCEDURE wladcy_kolejek (p_ilu INT);
END przychodnia;
/

CREATE OR REPLACE PACKAGE BODY przychodnia AS
  --funkcja sprawdzaj�ca, czy pesel zawiera wy��cznie cyfry (funkcja pomocnicza dla spr_pesel)
  FUNCTION isnumeric (p_napis CHAR)
    RETURN BOOLEAN
  AS
    l NUMBER;
  BEGIN
    IF INSTR(p_napis, ' ') != 0 OR p_napis IS NULL THEN
      RETURN FALSE;
    END IF;
    l := TO_NUMBER(p_napis);
    RETURN TRUE;
  EXCEPTION
    WHEN value_error
    THEN
      RETURN FALSE;
  END;
  
  --funkcja sprawdzaj�ca czy wszystkie napisy na li�cie zawieraj� wy��cznie cyfry
  FUNCTION isnumeric (p_napisy T_LISTA) 
    RETURN BOOLEAN
  AS
  BEGIN
    FOR v_i IN p_napisy.FIRST..p_napisy.LAST LOOP
      IF NOT isnumeric(p_napisy(v_i)) THEN
        RETURN FALSE;
      END IF;
    END LOOP;
    RETURN TRUE;
  END;
  
  --funkcja waliduj�ca poprawno�� numeru PESEL
  FUNCTION spr_pesel (p_pesel CHAR)
  RETURN BOOLEAN AS
    e_bledny_pesel EXCEPTION;
    TYPE waga IS TABLE OF NUMERIC(1);
    wagi waga := waga(1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1);
    pozycja PLS_INTEGER;
    wynik NUMBER := 0;
  BEGIN
    IF p_pesel IS NULL THEN RETURN TRUE; END IF; --obcokrajowcy (funkcja b�dzie u�yta w wyzwalaczu, wi�c pusty musi by� poprawny
      IF (LENGTH(p_pesel) = 11 AND isnumeric(p_pesel)) THEN
      FOR pozycja in 1..11 LOOP
        wynik := wynik + TO_NUMBER(SUBSTR(p_pesel, pozycja, 1)) * wagi(pozycja);
      END LOOP;
      IF (MOD(wynik, 10) = 0) THEN
        RETURN TRUE;
      ELSE
         RAISE e_bledny_pesel;
      END IF;
    ELSE
      RAISE e_bledny_pesel;  --tak, dodany na si��
    END IF;
    EXCEPTION
      WHEN e_bledny_pesel THEN
        RETURN FALSE;
  END;
  
  --sprawdza, czy tabela istnieje (funkcja potrzebna do procedury wypisywania pacjenta)
  --NALE�Y PAMI�TA� O NADANIU UPRAWNIE� (OPIS W PLIKU "UWAGI.txt")
  FUNCTION czy_istnieje(p_nazwa_tabeli VARCHAR2)
  RETURN BOOLEAN AS
    v_count INT;
  BEGIN
    SELECT liczba INTO v_count 
    FROM (
      SELECT COUNT(table_name) AS liczba FROM sys.dba_tables WHERE table_name=UPPER(p_nazwa_tabeli)
    );
    IF v_count = 1 THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END;

  --zwraca p�e� na podstawie nr pesel
  FUNCTION jaka_plec(p_pesel pacjent.pesel%TYPE)
  RETURN CHAR AS
  BEGIN
    IF p_pesel IS NULL THEN
      RETURN '?';
    ELSIF MOD(TO_NUMBER(SUBSTR(p_pesel, 10, 1)), 2) = 0 THEN
      RETURN 'K';
    ELSE
        RETURN 'M';
    END IF;
  END;
  
  --zwraca dat� urodzenia na podstawie nr pesel
  FUNCTION data_urodzenia(p_pesel pacjent.pesel%TYPE)
  RETURN DATE AS
    v_dzien INT;
    v_miesiac INT;
    v_rok INT;
  BEGIN
    IF p_pesel IS NULL OR NOT spr_pesel(p_pesel) THEN
      RAISE_APPLICATION_ERROR(-20000, 'Nie mo�na wyci�gn�� daty urodzenia z pustego lub niepoprawnego numeru PESEL');
    ELSE 
      v_rok := SUBSTR(p_pesel, 1, 2);
      v_miesiac := SUBSTR(p_pesel, 3, 2);
      v_dzien := SUBSTR(p_pesel, 5, 2);
      CASE 
        WHEN v_miesiac > 80 THEN v_rok := 1800 + v_rok; v_miesiac := v_miesiac - 80;
        WHEN v_miesiac > 20 THEN v_rok := 2000 + v_rok; v_miesiac := v_miesiac - 20;
        ELSE v_rok := 1900 + v_rok;
      END CASE;
      RETURN TO_DATE(v_rok||'/'||v_miesiac||'/'||v_dzien, 'yyyy/mm/dd');
    END IF;
  END;
  
  
  PROCEDURE zarchiwizuj_dane_pacjenta(p_pacjent pacjent%ROWTYPE) AS
    c_nazwa_tabeli CONSTANT CHAR(12) := 'pacjent_arch';
  BEGIN
    IF NOT czy_istnieje(c_nazwa_tabeli) THEN
      --przy tworzeniu tabeli nie mo�na u�ywa� typ�w zakotwiczonych
      EXECUTE IMMEDIATE '
        CREATE TABLE pacjent_arch (
          id            NUMBER (11) NOT NULL PRIMARY KEY,
          pacjent_id    NUMBER (11) NOT NULL ,
          imie          VARCHAR2 (20) NOT NULL ,
          nazwisko      VARCHAR2 (30) NOT NULL ,
          pesel         CHAR (11) ,
          ubezpieczenie CHAR (1) NOT NULL ,
          tel           CHAR(9),
          adres_id      NUMBER (11) ,
          data_zmiany   DATE DEFAULT SYSDATE NOT NULL
        )
      '; --dynamiczny sql
      EXECUTE IMMEDIATE 'CREATE SEQUENCE pacjent_arch_id_SEQ START WITH 1 NOCACHE ORDER' ;
      EXECUTE IMMEDIATE '
        CREATE OR REPLACE TRIGGER pacjent_arch_id_TRG BEFORE
          INSERT ON pacjent_arch FOR EACH ROW WHEN (NEW.id IS NULL) BEGIN :NEW.id := pacjent_arch_id_SEQ.NEXTVAL;
        END;
      '; --dynamiczny sql (WYMAGANE DODATKOWE UPRAWNIENIA - SZCZEGӣY W PLIKU "UWAGI.txt")
    END IF;
    --wywo�anie wprost powodowa�o b��d kompilacji spowodowany brakiem tabeli pacjent_arch (przy pierwszym uruchomieniu)
    --w ten spos�b omin��em walidacj� tego fragmentu
    EXECUTE IMMEDIATE 'INSERT INTO pacjent_arch(pacjent_id, imie, nazwisko, pesel, ubezpieczenie, adres_id) VALUES (
      :pacjent_id,
      :imie,
      :nazwisko,
      :pesel,
      :ubezpieczenie,
      :adres_id
    )' USING p_pacjent.id, p_pacjent.imie, p_pacjent.nazwisko, p_pacjent.pesel, p_pacjent.ubezpieczenie, 
      p_pacjent.adres_id; --parametry dynamicznego sql(typy zakotwiczone niedozwolone)
  END;
  
  PROCEDURE zarchiwizuj_dane_pacjenta(p_pacjent_id pacjent.id%TYPE) AS
    c_nazwa_tabeli CONSTANT CHAR(12) := 'pacjent_arch';
    v_pacjent pacjent%ROWTYPE;
  BEGIN
  dbms_output.put_line('>>>debug1');
    IF NOT czy_istnieje(c_nazwa_tabeli) THEN
    dbms_output.put_line('>>>debug2');
      --przy tworzeniu tabeli nie mo�na u�ywa� typ�w zakotwiczonych
      EXECUTE IMMEDIATE '
        CREATE TABLE pacjent_arch (
          id            NUMBER (11) NOT NULL PRIMARY KEY,
          pacjent_id    NUMBER (11) NOT NULL ,
          imie          VARCHAR2 (20) NOT NULL ,
          nazwisko      VARCHAR2 (30) NOT NULL ,
          pesel         CHAR (11) ,
          ubezpieczenie CHAR (1) NOT NULL ,
          tel           CHAR(9),
          adres_id      NUMBER (11) ,
          data_zmiany   DATE DEFAULT SYSDATE NOT NULL
        )
      '; --dynamiczny sql
      dbms_output.put_line('>>>debug3');
      EXECUTE IMMEDIATE 'CREATE SEQUENCE pacjent_arch_id_SEQ START WITH 1 NOCACHE ORDER' ;
      EXECUTE IMMEDIATE '
        CREATE OR REPLACE TRIGGER pacjent_arch_id_TRG BEFORE
          INSERT ON pacjent_arch FOR EACH ROW WHEN (NEW.id IS NULL) BEGIN :NEW.id := pacjent_arch_id_SEQ.NEXTVAL;
        END;
      '; --dynamiczny sql (WYMAGANE DODATKOWE UPRAWNIENIA - SZCZEGӣY W PLIKU "UWAGI.txt")
      dbms_output.put_line('>>>debug4');
    END IF;
    dbms_output.put_line('>>>debug5');
    SELECT * INTO v_pacjent FROM pacjent WHERE id=p_pacjent_id;
    dbms_output.put_line('>>>debug6');
    --wywo�anie wprost powodowa�o b��d kompilacji spowodowany brakiem tabeli pacjent_arch (przy pierwszym uruchomieniu)
    --w ten spos�b omin��em walidacj� tego fragmentu
    EXECUTE IMMEDIATE 'INSERT INTO pacjent_arch(pacjent_id, imie, nazwisko, pesel, ubezpieczenie, adres_id) VALUES (
      :pacjent_id,
      :imie,
      :nazwisko,
      :pesel,
      :ubezpieczenie,
      :adres_id
    )' USING v_pacjent.id, v_pacjent.imie, v_pacjent.nazwisko, v_pacjent.pesel, v_pacjent.ubezpieczenie, 
      v_pacjent.adres_id; --parametry dynamicznego sql(typy zakotwiczone niedozwolone)
      dbms_output.put_line('>>>debug7');
  END;
  
  PROCEDURE udzial_plci AS
    v_liczba_mezczyzn INT := 0;
    v_liczba_kobiet INT := 0;
    v_niezdefiniowano INT := 0;
    v_liczba_pacjentow INT;
    v_plec CHAR(1);
  BEGIN
    SELECT count(id) INTO v_liczba_pacjentow FROM pacjent;
    FOR v_pacjent IN (SELECT pesel FROM pacjent) LOOP --niejawny kursor (bez bawienia si� z open-fetch-close i deklarowanie zmiennej)
      v_plec := jaka_plec(v_pacjent.pesel);
      IF v_plec = 'K' THEN
        v_liczba_kobiet := v_liczba_kobiet + 1;
      ELSIF v_plec = 'M' THEN
        v_liczba_mezczyzn := v_liczba_mezczyzn + 1;
      ELSE
        v_niezdefiniowano := v_niezdefiniowano + 1;
      END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('KOBIET'||CHR(9)||'MʯCZYZN'||CHR(9)||'NIE OKRE�LONO');
    DBMS_OUTPUT.PUT_LINE('---------------------------------');
    DBMS_OUTPUT.PUT_LINE(RPAD(ROUND(v_liczba_kobiet*100/v_liczba_pacjentow, 2)||'%', 6, ' ')||CHR(9)||
      RPAD(ROUND(v_liczba_mezczyzn*100/v_liczba_pacjentow, 2)||'%', 9, ' ')||CHR(9)||
      ROUND(v_niezdefiniowano*100/v_liczba_pacjentow, 2)||'%');
  END;
  
  PROCEDURE podwyzka_piel (p_podwyzka IN OUT NUMERIC) AS
    CURSOR cur_podwyzka IS SELECT * FROM stanowisko WHERE nazwa LIKE 'piel�gniarka%' FOR UPDATE;
    v_stanowisko stanowisko%ROWTYPE;
  BEGIN
    OPEN cur_podwyzka; --dla wprawy tym razem wprost
    LOOP
      FETCH cur_podwyzka INTO v_stanowisko;
      EXIT WHEN cur_podwyzka%NOTFOUND;
      UPDATE stanowisko 
      SET podstawa = podstawa+p_podwyzka
      WHERE CURRENT OF cur_podwyzka;
    END LOOP;
    CLOSE cur_podwyzka;
    SELECT MAX(podstawa) INTO p_podwyzka FROM (SELECT * FROM stanowisko WHERE nazwa LIKE 'piel�gniarka%');
  END;
  
  PROCEDURE odejmij_szczepionke (p_nazwa IN szczepionka.nazwa%TYPE, p_komunikat OUT VARCHAR2) AS
    v_ilosc szczepionka.ilosc%TYPE;
  BEGIN
    SELECT ilosc INTO v_ilosc FROM szczepionka WHERE nazwa = p_nazwa;
    IF v_ilosc > 0 THEN
      UPDATE szczepionka SET ilosc = ilosc-1 WHERE nazwa = p_nazwa;
      p_komunikat := 'Pozosta�o '||(v_ilosc-1)||' sztuk';
    ELSE
      p_komunikat := 'Dana szczepionka ju� si� sko�czy�a';
    END IF;
  END;
  
  PROCEDURE wladcy_kolejek (p_ilu INT) AS
    v_tytul VARCHAR(5);
  BEGIN
    FOR v_ancymon IN (
      SELECT nazwisko, imie, pesel, wizyt 
      FROM (
        SELECT p.id, nazwisko, imie, pesel, COUNT(p.id) AS wizyt 
        FROM wizyta w 
        JOIN pacjent p 
        ON w.pacjent_id = p.id 
        GROUP BY p.id, nazwisko, imie, pesel 
        ORDER BY wizyt DESC
      ) WHERE ROWNUM <= p_ilu) LOOP
        IF jaka_plec(v_ancymon.pesel) = 'K' THEN
          v_tytul := 'Pani ';
        ELSIF jaka_plec(v_ancymon.pesel) = 'M' THEN
          v_tytul := 'Pan ';
        ELSE
          v_tytul := '';
        END IF;
        DBMS_OUTPUT.PUT_LINE(v_tytul||v_ancymon.nazwisko||' '||v_ancymon.imie||': '||v_ancymon.wizyt||' wizyt');
      END LOOP;
  END;
END przychodnia;
/

------------------------------------------------------------------------------------------------------------------------
--                                             TESTY PAKIETU PRZYCHODNIA                                              --
------------------------------------------------------------------------------------------------------------------------

--sprawdzenie poprawno�ci dzia�ania funkcji isnumeric
DECLARE
   TYPE napis IS TABLE OF VARCHAR2(20);
   napisy napis := napis('1234', '123a', 'abcd', NULL);
   i PLS_INTEGER;
BEGIN
   FOR i in napisy.FIRST..napisy.LAST LOOP
    IF przychodnia.isnumeric(napisy(i)) THEN
      DBMS_OUTPUT.PUT_LINE(napisy(i)||' to liczba');
    ELSE
      DBMS_OUTPUT.PUT_LINE(napisy(i)||' to napis');
    END IF;
   END LOOP;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji isnumeric dla list
DECLARE
   napisy1 przychodnia.t_lista := przychodnia.t_lista('132', '3345435325', '34234', '0');
   napisy2 przychodnia.t_lista := przychodnia.t_lista('354325', '355345', '444xxx342', '23423', '32543');
BEGIN
    IF przychodnia.isnumeric(napisy1) THEN
      DBMS_OUTPUT.PUT_LINE('Lista napisy1 zawiera wy��cznie liczby');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Lista napisy1 zawiera napisy');
    END IF;
    
    IF przychodnia.isnumeric(napisy2) THEN
      DBMS_OUTPUT.PUT_LINE('Lista napisy2 zawiera wy��cznie liczby');
    ELSE
      DBMS_OUTPUT.PUT_LINE('Lista napisy2 zawiera napisy');
    END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji spr_pesel
DECLARE
  TYPE pesel IS TABLE OF VARCHAR2(20);
  pesele pesel := pesel(
    '123456789012', --niepoprawny(za d�ugi)
    '123456789',    --niepoprawny (za kr�tki)
    '12345678abc',  --niepoprawny (napis)
    '12345678901',  --niepoprawny
    '47020409998',  --poprawny
    '73031613189',  --poprawny
    NULL,           --poprawny (obcokrajowcy)
    '07232510042',  --poprawny
    '63011108697',  --poprawny
    '23093012818'   --niepoprawny
  );
  i PLS_INTEGER;
BEGIN
   FOR i in pesele.FIRST..pesele.LAST LOOP
    IF przychodnia.spr_pesel(pesele(i)) THEN
      DBMS_OUTPUT.PUT_LINE(pesele(i)||' jest poprawny');
    ELSE
      DBMS_OUTPUT.PUT_LINE(pesele(i)||' jest niepoprawny');
    END IF;
   END LOOP;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji czy_istnieje
BEGIN
  IF przychodnia.czy_istnieje('pacjent') THEN
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent istnieje');
  END IF;
  IF NOT przychodnia.czy_istnieje('aoiwerpcwiour') THEN
    DBMS_OUTPUT.PUT_LINE('Podana tabela nie istnieje');
  END IF;
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji jaka_plec
BEGIN
  DBMS_OUTPUT.PUT_LINE('69062414266 => '||przychodnia.jaka_plec('69062414266'));
  DBMS_OUTPUT.PUT_LINE('97081906259 => '||przychodnia.jaka_plec('97081906259'));
  DBMS_OUTPUT.PUT_LINE('brak        => '||przychodnia.jaka_plec(NULL));
END;
/

--sprawdzenie poprawno�ci dzia�ania funkcji data_urodzenia
BEGIN
  DBMS_OUTPUT.PUT_LINE('Osoba o numerze PESEL 12920793018 urodzi�a si�: '||
    TO_CHAR(przychodnia.data_urodzenia('12920793018'), 'dd-mm-yyyy'));
  DBMS_OUTPUT.PUT_LINE('Osoba o numerze PESEL 81040714542 urodzi�a si�: '||
    TO_CHAR(przychodnia.data_urodzenia('81040714542'), 'dd-mm-yyyy'));
  DBMS_OUTPUT.PUT_LINE('Osoba o numerze PESEL 03250810988 urodzi�a si�: '||
    TO_CHAR(przychodnia.data_urodzenia('03250810988'), 'dd-mm-yyyy'));
  DBMS_OUTPUT.PUT_LINE('Osoba o numerze PESEL NULL urodzi�a si�: '||przychodnia.data_urodzenia(NULL)); --b��d
END;
/

--sprawdzenie poprawno�ci dzia�ania procedury zarchiwizuj_dane_pacjenta
BEGIN
  IF przychodnia.czy_istnieje('pacjent_arch') THEN
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent_arch istnieje');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent_arch NIE istnieje');
  END IF;
END;
/

SELECT * FROM pacjent;
DECLARE
  v_pecjent pacjent%ROWTYPE;
BEGIN
  SELECT * INTO v_pecjent FROM pacjent WHERE id=1;
  przychodnia.zarchiwizuj_dane_pacjenta(v_pecjent);
END;
/

BEGIN
przychodnia.zarchiwizuj_dane_pacjenta(2); --test przeci��onej funkcji
END;
/

UPDATE pacjent SET nazwisko='AAABBBCCC' WHERE id=1;
SELECT * FROM pacjent;

BEGIN
  IF przychodnia.czy_istnieje('pacjent_arch') THEN
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent_arch istnieje');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Tabela pacjent_arch NIE istnieje');
  END IF;
END;
/
SELECT * FROM pacjent_arch;


--sprawdzenie poprawno�ci dzia�ania procedury udzial_plci
SELECT CASE 
  WHEN pesel IS NULL THEN 'brak' 
  ELSE pesel 
END AS pesel FROM pacjent;

EXECUTE przychodnia.udzial_plci;


--sprawdzenie poprawno�ci dzia�ania procedury podwyzka_piel
SELECT nazwa, podstawa FROM stanowisko;
SELECT pwz, pensja FROM zarobki_piel;
DECLARE
  v_param stanowisko.podstawa%TYPE := 400;
BEGIN
  przychodnia.podwyzka_piel(v_param);
  DBMS_OUTPUT.PUT_LINE('Po podwy�ce, najwy�sza podstawa wynosi: '||v_param);
END;
/
SELECT nazwa, podstawa FROM stanowisko;
SELECT pwz, pensja FROM zarobki_piel;


--sprawdzenie poprawno�ci dzia�ania procedury podwyzka_piel
DECLARE
  v_komunikat VARCHAR2(50);
BEGIN
  przychodnia.odejmij_szczepionke('Meningo A + C', v_komunikat);
  DBMS_OUTPUT.PUT_LINE(v_komunikat);
END;
/

--sprawdzenie poprawno�ci dzia�ania procedury wladcy_kolejek
EXECUTE przychodnia.wladcy_kolejek(1);
EXECUTE przychodnia.wladcy_kolejek(4);