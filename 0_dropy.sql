DROP VIEW zarobki_lekarz ;
DROP VIEW czestosc_szczep ;
DROP VIEW zarobki_piel ;
DROP VIEW pacjent_z_adresem ;
DROP VIEW udzial_plci_view ;

DROP TABLE adres CASCADE CONSTRAINTS ;
DROP TABLE diagnoza CASCADE CONSTRAINTS ;
DROP TABLE gabinet CASCADE CONSTRAINTS ;
DROP TABLE jednchor CASCADE CONSTRAINTS ;
DROP TABLE jednostka CASCADE CONSTRAINTS ;
DROP TABLE lek CASCADE CONSTRAINTS ;
DROP TABLE lek_naco CASCADE CONSTRAINTS ;
DROP TABLE lek_przeciwsk CASCADE CONSTRAINTS ;
DROP TABLE lekarz CASCADE CONSTRAINTS ;
DROP TABLE lekarz_spec CASCADE CONSTRAINTS ;
DROP TABLE lekarz_stan CASCADE CONSTRAINTS ;
DROP TABLE pacjent CASCADE CONSTRAINTS ;
DROP TABLE pielegniarka CASCADE CONSTRAINTS ;
DROP TABLE podanie CASCADE CONSTRAINTS ;
DROP TABLE rec_lek CASCADE CONSTRAINTS ;
DROP TABLE recepta CASCADE CONSTRAINTS ;
DROP TABLE skierowanie CASCADE CONSTRAINTS ;
DROP TABLE skierzabieg CASCADE CONSTRAINTS ;
DROP TABLE specjalizacja CASCADE CONSTRAINTS ;
DROP TABLE stanowisko CASCADE CONSTRAINTS ;
DROP TABLE szczep_naco CASCADE CONSTRAINTS ;
DROP TABLE szczep_przeciwsk CASCADE CONSTRAINTS ;
DROP TABLE szczepionka CASCADE CONSTRAINTS ;
DROP TABLE typgabinetu CASCADE CONSTRAINTS ;
DROP TABLE typwizyty CASCADE CONSTRAINTS ;
DROP TABLE typzabiegu CASCADE CONSTRAINTS ;
DROP TABLE wizyta CASCADE CONSTRAINTS ;
DROP TABLE wojewodztwo CASCADE CONSTRAINTS ;
DROP TABLE zabieg CASCADE CONSTRAINTS ;
DROP TABLE zamowic;

DROP TABLE pacjent_arch;

DROP SEQUENCE adres_id_seq;
DROP SEQUENCE gabinet_id_seq;
DROP SEQUENCE jednchor_id_seq;
DROP SEQUENCE jednostka_id_seq;
DROP SEQUENCE lek_id_seq;
DROP SEQUENCE lekarz_id_seq;
DROP SEQUENCE pacjent_id_seq;
DROP SEQUENCE pielegniarka_id_seq;
DROP SEQUENCE podanie_id_seq;
DROP SEQUENCE recepta_id_seq;
DROP SEQUENCE skierzabieg_id_seq;
DROP SEQUENCE skierowanie_id_seq;
DROP SEQUENCE specjalizacja_id_seq;
DROP SEQUENCE stanowisko_id_seq;
DROP SEQUENCE szczepionka_id_seq;
DROP SEQUENCE typgabinetu_id_seq;
DROP SEQUENCE typwizyty_id_seq;
DROP SEQUENCE typzabiegu_id_seq;
DROP SEQUENCE wizyta_id_seq;
DROP SEQUENCE wojewodztwo_id_seq;
DROP SEQUENCE zabieg_id_seq;
DROP SEQUENCE zamowic_id_seq;

DROP SEQUENCE skierowanie_numer_seq;
DROP SEQUENCE recepta_numer_seq;
DROP SEQUENCE skierzabieg_numer_seq;
DROP SEQUENCE pacjent_arch_id_SEQ;

DROP FUNCTION isnumeric;
DROP FUNCTION spr_pesel;
DROP FUNCTION czy_istnieje;
DROP FUNCTION jaka_plec;
DROP FUNCTION data_urodzenia;


DROP PROCEDURE zarchiwizuj_dane_pacjenta;
DROP PROCEDURE udzial_plci;
DROP PROCEDURE podwyzka_piel;
DROP PROCEDURE odejmij_szczepionke;
DROP PROCEDURE wladcy_kolejek;

DROP PACKAGE przychodnia;