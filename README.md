**Polskie nazwy narzucone w wymaganiach projektu**

Do prawidłowego działania projektu wymagane jest nadanie użytkownikowi system
dodatkowych uprawnień:

SQL> connect / as sysdba
SQL> grant select on sys.dba_tables to system;
SQL> grant all privileges to system; --w blokach nazwanych uprawnienia ról nie mają znaczenia
SQL> exit

------------------------------------------------------

## Testowe loginy i hasła do aplikacji

|login	|	hasło	|	status|
|--------|----------|---------------------|
|admin	|	admin	|	administrator|
|lekarz1	|	lekarz1	|	lekarz|
|piel1	|	piel	|	pielęgniarka|
|kadrowa1|	kadrowa	|	dział kadr|

## Scenariusz

Program powstał na zlecenie Niepublicznego Zakładu Opieki Zdrowotnej (NZOZ) wykonującej świadczenia w ramach Narodowego Funduszu Zdrowia (NFZ) i ma na celu wspomaganie pracy przychodni zapewniając dostęp do spójnej i zawsze aktualnej bazy danych dla pracowników zatrudnionych na różnych stanowiskach.

Najistotniejszą cechą z punktu widzenia zleceniodawcy jest kolor aplikacji - ma się ona komponować z bladozielonymi elementami w wystroju wnętrza i z takimi samymi akcentami na fartuchach pracowników przychodni. 

Z programu będą korzystać lekarze, pielęgniarki oraz osoba zarządzająca kadrami. 
W zależności od zajmowanego stanowiska, użytkownicy będą logować się do systemu za pomocą konta o odpowiadających mu uprawnieniach, uzyskując dostęp tylko do tych danych, do których ma uprawnienia. W systemie znajduje się również konto administratora (właściciela), które daje nieograniczony dostęp do wszystkich zgromadzonych danych.

Lekarze mają wgląd w dane pacjenta, w jego historię wizyt, zdiagnozowanych jednostek chorobowych oraz wystawionych skierowań i zaleceń. Nowa wizyta zostaje założona w gabinecie po podaniu nr PESEL pacjenta, niezależnie od rejestracji (rejestrowane są tylko wizyty, które doszły do skutku). Jeżeli pacjent nie ma aktualnego ubezpieczenia zdrowotnego, lekarz dostaje stosowną informację, aby mógł poinformować pacjenta o zaistniałej sytuacji i poinformować go o konsekwencjach.
Podczas wizyty lekarz może wystawić skierowanie na zabieg w gabinecie zabiegowym i tylko takie zabiegi mogą być w nim wykonywane (brak możliwości odpłatnego zlecania badań itp. przez pacjentów przychodni).

Pielęgniarka w gabinecie zabiegowym ma możliwość przeprowadzania bilansów, patronaży i prostych zabiegów takich jak pobranie krwi, przyjęcie próbek do wysyłki do laboratorium, wykonywania zastrzyków, podawania szczepień itp.
Zakłada się, że każdy czynność medyczna jest osobnym zabiegiem. Z tego powodu, przy jednej wizycie w gabinecie możliwe jest wykonanie kilku zabiegów, np. podanie dwóch szczepionek to dwa zabiegi, które wykonywane są jeden po drugim.

Osoba odpowiedzialna za kadry ma dostęp jedynie do podstawowych danych osób zatrudnionych w ośrodku zdrowia i odpowiada za zarządzanie kadrą i ich zarobkami.
Każde stanowisko ma przypisaną pensję zwaną podstawą, która następnie jest powiększana o indywidualne dodatki. Na ostateczną pensję składa się zatem suma powstała w wyniku dodania do siebie podstawy zgodniej z zajmowanym stanowiskiem i dodatku właściwego dla danego pracownika. Ponieważ lekarze mogą być w przychodni zatrudnienie na kilku stanowiskach, na ich pensję, poza dodatkiem, składa się suma podstaw dla każdego zajmowanego przez nich stanowiska , w przypadku niepełnego wymiaru godzin, przemnożona przez odpowiadającą zajmowanemu stanowisku część etatu.

Ponieważ priorytetem dla przychodni jest nieprzerwana praca gabinetu szczepień, gdy zapas jakiejś szczepionki spadnie poniżej 10 ampułek, zostaje wygenerowana informacja o potrzebie dokonania zamówienia. 

Kluczowe jest również zachowanie poprzednich danych o pacjentach, dlatego każdorazowa zmiana danych pacjenta powinna zostać zachowana do późniejszego wglądu.

Ze statystycznego punktu widzenia istotnymi dla zleceniodawcy informacjami są: zestawienie stanu zmagazynowanych szczepionek, pozwalające lepiej zarządzać pracą gabinetu szczepień informacja o liczbie wykonywanych zabiegów przypadających na jednego zapisanego do przychodni pacjenta oraz pozwalający lepiej dostosować do potrzeb podejmowane działania i lepiej dopasować ofertę do oczekiwań pacjentów - procentowy udział kobiet i mężczyzn wśród pacjentów przychodni.
